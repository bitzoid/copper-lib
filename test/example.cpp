#include <iostream>

#include "copper/copper.h"

#include <array>
#include <forward_list>
#include <list>
#include <map>
#include <memory>
#include <optional>
#include <set>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <variant>
#include <vector>

namespace copper {
  namespace config {
    template <> struct sep_bidy<std::list<bool>,void> { static constexpr char value[] = " and"; };
    template <> struct open_bidy<std::list<bool>,void> { static constexpr char value[] = "bools:"; };
    template <> struct close_bidy<std::list<bool>,void> { static constexpr char value = '.'; };
    template <> struct before_close<std::list<bool>,void> : std::integral_constant<char,0>{ };
  }
}

namespace {
  struct S {
    struct iterator {
      iterator(int);
      int operator*();
      iterator& operator++();
      //friend bool operator!=(iterator const&, iterator const&); <-- broken iterator type
    };
    iterator begin() { return iterator(0); }
    iterator end() { return iterator(1); }
    S(int) {}
  };

  //void foo(void*********) {}
}


int main() {
  static_assert(copper::detail::enable<S> == false);
  using copper::operator<<;
  float const arr[5] = { 0.1, 0.5, 100, -4.3, 1e3};
  std::array<char,9> hithere {"hi there"};
  int i = 42;

  std::cout << "pair                 " << std::make_pair<int,char>(0,'0') << "\n";
  std::cout << "tuple                " << std::make_tuple<int,float,short,long,double>(0,0,10,7,4) << "\n";
  std::cout << "tuple<>              " << std::make_tuple<>() << "\n";
  std::cout << "float[5]             " << arr << "\n";
  std::cout << "array                " << std::array<int,5>{0,0,10,7,4} << "\n";
  std::cout << "\n";
  std::cout << "vector               " << std::vector<int>{0,0,10,7,4} << "\n";
  std::cout << "list                 " << std::list<int>{0,0,10,7,4} << "\n";
  std::cout << "forward_list         " << std::forward_list<int>{0,0,10,7,4} << "\n";
  std::cout << "\n";
  std::cout << "set                  " << std::set<int>{0,0,10,7,4} << "\n";
  std::cout << "unordered_set        " << std::unordered_set<int>{0,0,10,7,4} << "\n";
  std::cout << "multiset             " << std::multiset<int>{0,0,10,7,4} << "\n";
  std::cout << "unordered_multiset   " << std::unordered_multiset<int>{0,0,10,7,4} << "\n";
  std::cout << "map                  " << std::map<int,float>{std::make_pair(0,100.1),std::make_pair(1,101.1)} << "\n";
  std::cout << "unordered_map        " << std::unordered_map<int,float>{std::make_pair(0,100.1),std::make_pair(1,101.1)} << "\n";
  std::cout << "list<bool> (custom)  " << std::list<bool>{false, true} << "\n";
  std::cout << "\n";
  std::cout << "unique_ptr           " << std::make_unique<int>(42) << "\n";
  std::cout << "unique_ptr(0)        " << std::unique_ptr<int>() << "\n";
  std::cout << "optional             " << std::optional<int>(42) << "\n";
  std::cout << "nullopt              " << std::optional<int>(std::nullopt) << "\n";
  std::cout << "nullptr (default)    " << nullptr << "\n"; // not done by this library but by std::nullptr_t
  std::cout << "int*                 " << &i << "\n";
  std::cout << "void* (default)      " << static_cast<void*>(&i) << "\n";
  std::cout << "\n";
  std::cout << std::map<int,std::tuple<std::optional<std::multiset<char>>,std::list<std::array<char,9>>,std::set<float>>>
               { std::make_pair(0,std::make_tuple(std::optional<std::multiset<char>>(std::multiset<char>{'x'}),std::list<std::array<char,9>>{hithere,hithere},std::set<float>{0,0.5}))
               , std::make_pair(1,std::make_tuple(std::optional<std::multiset<char>>(std::multiset<char>{'y'}),std::list<std::array<char,9>>{hithere},std::set<float>{1,0.5}))} << "\n";
}
