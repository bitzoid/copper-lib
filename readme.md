# copper -- COntainer Pretty PrintER lib

> Copyright (C) 2020  bitzoid
> 
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
> 
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
> 
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <http://www.gnu.org/licenses/>.

This one-header-only library is inspired in its purpose by the cxx-prettyprint library, but generalises containers.
So, it doesn't only work for std:: containers but for any container that *looks* like a set, vector, etc.

## Usage
To prevent bleeing into the top level namespace, this library only defines names in the `copper` namespace. In order to make

    std::cout << my_container << "\n";

work, you have to have a `using copper::operator<<;` or a `using namespace copper;`. Preferably at function scope.
It is guaranteed that the copper namespace only contains an operator<< free function and a (private) detail namespace (which contains the implementation).
