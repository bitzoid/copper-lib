#pragma once

/*
 *  copper lib -- COntainer Pretty PrintER lib
 *  Copyright (C) 2020  bitzoid
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 * This one-header-only library is inspired in its purpose by the cxx-prettyprint library, but generalises containers.
 * So, it doesn't only work for std:: containers but for any container that *looks* like a set, vector, etc.
 */

#include <type_traits>
#include <iterator>
#include <tuple>

namespace copper {

  /* Specialise these with the second argument set to void in your compilation unit to customise the library's behaviour. */
  namespace config {
    template <typename T, typename = void> struct enable_indirectiony : std::true_type {};
    template <typename T, typename = void> struct special_stringy : std::true_type {};

    template <typename T, typename = void> struct after_sep : std::integral_constant<char,' '> { };
    template <typename T, typename = void> struct before_sep : std::integral_constant<char,0> { };
    template <typename T, typename = void> struct after_open : std::integral_constant<char,' '> { };
    template <typename T, typename = void> struct before_close : std::integral_constant<char,' '> { };

    template <typename T, typename = void> struct start : std::integral_constant<char,0> { };
    template <typename T, typename = void> struct finish : std::integral_constant<char,0> { };

    template <typename T, typename = void> struct sep_tuply : std::integral_constant<char,','> { };
    template <typename T, typename = void> struct sep_tuply2 : std::integral_constant<char,':'> { };
    template <typename T, typename = void> struct sep_usety : std::integral_constant<char,','> { };
    template <typename T, typename = void> struct sep_ssety { static constexpr char value[] = " <"; };
    template <typename T, typename = void> struct sep_mssety { static constexpr char value[] = " <="; };
    template <typename T, typename = void> struct sep_ramy : std::integral_constant<char,','> { };
    template <typename T, typename = void> struct sep_bidy { static constexpr char value[] = " <->"; };
    template <typename T, typename = void> struct sep_fwdy { static constexpr char value[] = " ->"; };

    template <typename T, typename = void> struct null_indirectiony { static constexpr char value[] = "null"; };

    template <typename T, typename = void> struct open_tuply : std::integral_constant<char,'('> { };
    template <typename T, typename = void> struct close_tuply : std::integral_constant<char,')'> { };

    template <typename T, typename = void> struct open_stringy : std::integral_constant<char,'"'> { };
    template <typename T, typename = void> struct close_stringy : std::integral_constant<char,'"'> { };

    template <typename T, typename = void> struct open_indirectiony : std::integral_constant<char,'*'> { };
    template <typename T, typename = void> struct close_indirectiony : std::integral_constant<char,0> { };

    template <typename T, typename = void> struct open_iteratory : std::integral_constant<char,'['> { };
    template <typename T, typename = void> struct close_iteratory : std::integral_constant<char,']'> { };
    template <typename T, typename = void> struct open_ramy : open_iteratory<T,void> {};
    template <typename T, typename = void> struct close_ramy : close_iteratory<T,void> {};
    template <typename T, typename = void> struct open_bidy : open_iteratory<T,void> {};
    template <typename T, typename = void> struct close_bidy : close_iteratory<T,void> {};
    template <typename T, typename = void> struct open_fwdy : open_iteratory<T,void> {};
    template <typename T, typename = void> struct close_fwdy : close_iteratory<T,void> {};

    template <typename T, typename = void> struct open_sety : std::integral_constant<char,'{'> { };
    template <typename T, typename = void> struct close_sety : std::integral_constant<char,'}'> { };
    template <typename T, typename = void> struct open_ssety : open_sety<T,void> {};
    template <typename T, typename = void> struct close_ssety : close_sety<T,void> {};
    template <typename T, typename = void> struct open_mssety : open_ssety<T,void> {};
    template <typename T, typename = void> struct close_mssety : close_ssety<T,void> {};
    template <typename T, typename = void> struct open_usety : open_sety<T,void> {};
    template <typename T, typename = void> struct close_usety : close_sety<T,void> {};
  }

  namespace detail {
    namespace trait_impl {
      template <typename C> constexpr C& mk_ref();
      template <typename C> constexpr C* mk_ptr();

      template <typename... Args>
      struct find_non_void { };
      template <>
      struct find_non_void<> { using type = void; };
      template <typename... Args>
      struct find_non_void<void,Args...> : find_non_void<Args...> {};
      template <typename Arg, typename... Args>
      struct find_non_void<Arg,Args...> { using type = Arg; };

      template <typename... Args>
      using find_non_void_t = typename find_non_void<Args...>::type;

      template <typename T> constexpr bool has_value_f(std::decay_t<decltype(T::value)>*) { return true; }
      template <typename T> constexpr bool has_value_f(...                              ) { return false; }

      template <typename T> constexpr bool has_key_type_f(std::decay_t<typename T::key_type>*) { return true; }
      template <typename T> constexpr bool has_key_type_f(...                                ) { return false; }

      template <typename T> constexpr bool has_key_compare_f(std::decay_t<typename T::key_compare>*) { return true; }
      template <typename T> constexpr bool has_key_compare_f(...                                   ) { return false; }

      template <typename T> constexpr bool insert_can_fail_f(std::decay_t<decltype(mk_ref<T>().insert(mk_ref<typename T::value_type>()).second)>*)
                                           { return std::is_same_v<bool,std::decay_t<decltype(mk_ref<T>().insert(mk_ref<typename T::value_type>()).second)>>; }
      template <typename T> constexpr bool insert_can_fail_f(...                                                                                 )
                                           { return false; }

      template <typename T>                struct is_sized_array_h       : std::false_type {};
      template <typename T, std::size_t N> struct is_sized_array_h<T[N]> : std::true_type {};

      namespace op {
        template <typename T> constexpr bool deref_f(std::decay_t<decltype(*mk_ref<T>())>*) { return true; }
        template <typename T> constexpr bool deref_f(...                                  ) { return false; }
        template <typename T> constexpr bool deref = deref_f<T>(nullptr);

        template <typename T> constexpr bool ineql_f(std::decay_t<decltype(bool(mk_ref<T>() != mk_ref<T>()))>*) { return true; }
        template <typename T> constexpr bool ineql_f(...                                                      ) { return false; }
        template <typename T> constexpr bool ineql = ineql_f<T>(nullptr);

        template <typename T> constexpr bool prepp_f(std::decay_t<decltype(++mk_ref<T>())>*) { return true; }
        template <typename T> constexpr bool prepp_f(...                                   ) { return false; }
        template <typename T> constexpr bool prepp = prepp_f<T>(nullptr);

        template <typename T> constexpr bool boolc_f(std::decay_t<decltype(bool(mk_ref<T>()))>*) { return true; }
        template <typename T> constexpr bool boolc_f(...                                       ) { return false; }
        template <typename T> constexpr bool boolc = ineql_f<T>(nullptr);
      }

      constexpr bool is_empty_string(char c) { return !c; }
      template <std::size_t N> constexpr bool is_empty_string(char const (& c)[N]) { return (N==0) || is_empty_string(c[0]); }
      template <typename T> constexpr bool is_empty_string(T const&) { return false; }

      template <typename C> constexpr bool has_begin_f(decltype(std::begin(mk_ref<C>()))*) { return true; }
      template <typename C> constexpr bool has_begin_f(...                               ) { return false; }

      template <typename C> constexpr bool has_end_f(decltype(std::end(mk_ref<C>()))*) { return true; }
      template <typename C> constexpr bool has_end_f(...                             ) { return false; }

      template <typename C> constexpr auto begin_ptr_t_f(std::decay_t<decltype(std::begin(mk_ref<C>()))>*)
                                           { return static_cast<std::decay_t<decltype(std::begin(mk_ref<C>()))>*>(nullptr); }
      template <typename C> constexpr auto begin_ptr_t_f(...                                             )
                                           { return static_cast<void*>(nullptr); }

      template <typename C> using begin_type = std::decay_t<std::remove_pointer_t<decltype(begin_ptr_t_f<C>(nullptr))>>;

      template <typename C> auto value_type_f(std::remove_reference_t<decltype(*mk_ref<begin_type<C>>())>*) -> std::remove_reference_t<decltype(*mk_ref<begin_type<C>>())>;
      template <typename C> void value_type_f(...                                                         );

      template <typename C> using value_type = std::decay_t<decltype(value_type_f<C>(nullptr))>;

      template <typename C> using iterator_type = std::conditional_t<
        op::deref<begin_type<C>>  &&  op::ineql<begin_type<C>>  &&  op::prepp<begin_type<C>>
      ,begin_type<C>,void>;

      template <typename C> constexpr auto iterator_category_membr_f(typename iterator_type<C>::iterator_category*)
                                           { return typename iterator_type<C>::iterator_category(); }
      template <typename C> constexpr void iterator_category_membr_f(...) { }

      template <typename C> constexpr auto iterator_category_trait_f(typename std::iterator_traits<iterator_type<C>>::iterator_category*)
                                           { return typename std::iterator_traits<iterator_type<C>>::iterator_category(); }
      template <typename C> constexpr void iterator_category_trait_f(...) { }

      template <typename C> using iterator_category = find_non_void_t<decltype(iterator_category_membr_f<C>(nullptr)),decltype(iterator_category_trait_f<C>(nullptr))>;
    }

    template <typename C> static constexpr bool has_iterator = !std::is_same_v<void,trait_impl::iterator_type<C>>;

    template <typename C> static constexpr bool has_iterator_category = has_iterator<C> && !std::is_same_v<void,trait_impl::iterator_category<C>>;

    template <typename C> using iterator_category = trait_impl::iterator_category<C>;

    template <typename C> using iterator_type = trait_impl::iterator_type<C>;

    template <typename C> static constexpr bool is_iter_fwd = has_iterator_category<C> && std::is_same_v<iterator_category<C>,std::forward_iterator_tag>;
    template <typename C> static constexpr bool is_iter_bid = has_iterator_category<C> && std::is_same_v<iterator_category<C>,std::bidirectional_iterator_tag>;
    template <typename C> static constexpr bool is_iter_ram = has_iterator_category<C> && std::is_same_v<iterator_category<C>,std::random_access_iterator_tag>;

    template <typename C> static constexpr bool has_begin_end = trait_impl::has_begin_f<C>(nullptr) && trait_impl::has_end_f<C>(nullptr);

    template <typename C> constexpr bool has_tuple_size = trait_impl::has_value_f<std::tuple_size<C>>(nullptr);


    template <typename C> constexpr bool is_tuply = has_tuple_size<C>;

    template <typename C> constexpr bool is_raw_sz = trait_impl::is_sized_array_h<C>::value;
    template <typename C> constexpr bool is_variably = (has_begin_end<C> && has_iterator<C>) && !is_raw_sz<C>;
    template <typename C> constexpr bool is_iteratory = is_raw_sz<C> || is_variably<C>;

    template <typename C> constexpr bool is_indirectiony = trait_impl::op::deref<C> && trait_impl::op::boolc<C>;

    template <typename C> constexpr bool is_stricty = trait_impl::insert_can_fail_f<std::remove_cv_t<C>>(nullptr);
    template <typename C> constexpr bool is_stringy = std::is_same_v<std::decay_t<trait_impl::value_type<C>>,char>;
    template <typename C> constexpr bool is_sety = trait_impl::has_key_type_f<C>(nullptr);
    template <typename C> constexpr bool is_sorty = trait_impl::has_key_compare_f<C>(nullptr);
    template <typename C> constexpr bool is_ssety = is_sety<C> && is_sorty<C>;
    template <typename C> constexpr bool is_usety = is_sety<C> && !is_sorty<C>;


    template <typename C> static constexpr bool enable = is_iteratory<C> || is_tuply<C> || (config::enable_indirectiony<C>::value && is_indirectiony<C>);


    template <
      typename Stream,
      typename C,
      std::size_t I,
      std::size_t N,
      typename = std::enable_if_t<is_tuply<C>>
    > inline
    void print_inner_tuply(Stream& stream, C& container) {
      if constexpr (I < N) {
        stream << std::get<I>(container);
      }
      if constexpr ((I+1) < N) {
        if constexpr (N == 2) {
          stream << config::before_sep<std::remove_cv_t<C>>::value << config::sep_tuply2<std::remove_cv_t<C>>::value << config::after_sep<std::remove_cv_t<C>>::value;
        } else {
          stream << config::before_sep<std::remove_cv_t<C>>::value << config::sep_tuply<std::remove_cv_t<C>>::value << config::after_sep<std::remove_cv_t<C>>::value;
        }
        print_inner_tuply<Stream,C,(I+1),N>(stream,container);
      }
    }

    template <
      typename Stream,
      typename C,
      typename SepGen,
      typename = std::enable_if_t<is_iteratory<C>>
    > inline
    void print_inner_iteratory(Stream& stream, iterator_type<C> begin, iterator_type<C> end) {
      if (begin != end) {
        stream << *begin;
        for (auto it = std::next(begin); it != end; ++it) {
          stream << config::before_sep<std::remove_cv_t<C>>::value << SepGen::value << config::after_sep<std::remove_cv_t<C>>::value;
          stream << *it;
        }
      }
    }

    template <
      typename Stream,
      typename C,
      typename = std::enable_if_t<is_stringy<C>>
    > inline
    void print_inner_stringy(Stream& stream, iterator_type<C> begin, iterator_type<C> end) {
      for (auto it = begin; it != end; ++it) {
        stream << *it;
      }
    }

    template <
      typename Stream,
      typename C,
      typename Null,
      typename = std::enable_if_t<is_indirectiony<C>>
    > inline
    void print_inner_indirectiony(Stream& stream, C const& ptr) {
      if (static_cast<bool>(ptr)) {
        stream << *ptr;
      } else {
        stream << Null::value;
      }
    }

    template <
      typename Stream,
      typename T,
      typename Open,
      typename After = config::after_open<T>
    > inline
    void open(Stream& stream) {
      if constexpr (!trait_impl::is_empty_string(Open::value)) {
        stream << Open::value << After::value;
      }
    }
    template <
      typename Stream,
      typename T,
      typename Close,
      typename Before = config::before_close<T>
    > inline
    void close(Stream& stream) {
      if constexpr (!trait_impl::is_empty_string(Close::value)) {
        stream << Before::value << Close::value;
      }
    }

    template <
      typename Stream,
      typename C,
      typename = std::enable_if_t<enable<C>>
    > inline
    void print(Stream& stream, C& container) {
      using T = std::remove_cv_t<C>;
      stream << config::start<T>::value;
      constexpr bool stringy = config::special_stringy<T>::value && is_stringy<C>;
      if constexpr (is_tuply<C> && !stringy) {
        open<Stream,T,config::open_tuply<T>>(stream);
        print_inner_tuply<Stream,C,0,std::tuple_size<C>::value>(stream,container);
        close<Stream,T,config::close_tuply<T>>(stream);
      } else if constexpr (stringy) {
        open<Stream,T,config::open_stringy<T>,std::integral_constant<char,0>>(stream);
        print_inner_stringy<Stream,C>(stream,std::begin(container),std::end(container));
        close<Stream,T,config::close_stringy<T>,std::integral_constant<char,0>>(stream);
      } else if constexpr (is_raw_sz<C>) {
        open<Stream,T,config::open_tuply<T>>(stream);
        print_inner_iteratory<Stream,C,config::sep_ramy<T>>(stream,std::begin(container),std::end(container));
        close<Stream,T,config::close_tuply<T>>(stream);
      } else if constexpr (is_ssety<C> && is_stricty<C>) {
        open<Stream,T,config::open_ssety<T>>(stream);
        print_inner_iteratory<Stream,C,config::sep_ssety<T>>(stream,std::begin(container),std::end(container));
        close<Stream,T,config::close_ssety<T>>(stream);
      } else if constexpr (is_ssety<C> && !is_stricty<C>) {
        open<Stream,T,config::open_mssety<T>>(stream);
        print_inner_iteratory<Stream,C,config::sep_mssety<T>>(stream,std::begin(container),std::end(container));
        close<Stream,T,config::close_mssety<T>>(stream);
      } else if constexpr (is_usety<C>) {
        open<Stream,T,config::open_usety<T>>(stream);
        print_inner_iteratory<Stream,C,config::sep_usety<T>>(stream,std::begin(container),std::end(container));
        close<Stream,T,config::close_usety<T>>(stream);
      } else if constexpr (is_iter_ram<C>) {
        open<Stream,T,config::open_ramy<T>>(stream);
        print_inner_iteratory<Stream,C,config::sep_ramy<T>>(stream,std::begin(container),std::end(container));
        close<Stream,T,config::close_ramy<T>>(stream);
      } else if constexpr (is_iter_bid<C>) {
        open<Stream,T,config::open_bidy<T>>(stream);
        print_inner_iteratory<Stream,C,config::sep_bidy<T>>(stream,std::begin(container),std::end(container));
        close<Stream,T,config::close_bidy<T>>(stream);
      } else if constexpr (is_iter_fwd<C>) {
        open<Stream,T,config::open_fwdy<T>>(stream);
        print_inner_iteratory<Stream,C,config::sep_fwdy<T>>(stream,std::begin(container),std::end(container));
        close<Stream,T,config::close_fwdy<T>>(stream);
      } else if constexpr (config::enable_indirectiony<C>::value && is_indirectiony<C>) {
        open<Stream,T,config::open_indirectiony<T>>(stream);
        print_inner_indirectiony<Stream,C,config::null_indirectiony<T>>(stream,container);
        close<Stream,T,config::close_indirectiony<T>>(stream);
      }
      stream << config::finish<T>::value;
    }
  }

  template <
    typename Stream,
    typename Container,
    typename = std::enable_if_t<copper::detail::enable<Container const>>
  > inline
  Stream& operator<<(Stream& stream, Container const& container) {
    copper::detail::print<Stream,Container const>(stream,container);
    return stream;
  }
}
